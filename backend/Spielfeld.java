package backend;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

import java.util.Random;

public class Spielfeld {
    public int [] zahlenF;
    double summe=0;
    Gewinn gewinn;
    public Spielfeld(Gewinn gewinn){
        this.gewinn = gewinn;
    }

    public Image sieben = new Image(getClass().getResourceAsStream("/picture/sieben.jpg"));
    public Image kirschen = new Image(getClass().getResourceAsStream("/picture/Kirschen.jpg"));
    public Image orange  = new Image(getClass().getResourceAsStream("/picture/Orange.jpg"));
    public Image melone = new Image(getClass().getResourceAsStream("/picture/Melonen.jpg"));
    public Image glocke  = new Image(getClass().getResourceAsStream("/picture/Glocke.png"));


    public int[] randomZahlen1bis9(){
        Random zufall = new Random();
        int maxZahl =100000;
         zahlenF = new int[9];
        for(int i =0;i< zahlenF.length;i++){
            zahlenF[i]=zufall.nextInt(maxZahl);
        }
        return zahlenF ;
    }
    public GridPane tauschFeld() {
        randomZahlen1bis9();
        int k = 0;
        GridPane gp = new GridPane();
        for (int i = 0; i < gewinn.feld.length; i++) {
            for (int j = 0; j < gewinn.feld[i].length; j++) {

                if (zahlenF[k] > 70000) {
                    k++;
                    gewinn.feld[i][j] = "kirschen";
                    gp.add(new ImageView(kirschen), j, i);

                } else if (zahlenF[k] < 69999 & zahlenF[k] > 50000) {
                    k++;
                    gewinn.feld[i][j] = "orange";

                    gp.add(new ImageView(orange), j, i);

                } else if (zahlenF[k] < 59999 & zahlenF[k] > 30000) {
                    k++;
                    gewinn.feld[i][j] = "glocke";
                    gp.add(new ImageView(glocke), j, i);

                } else if (zahlenF[k] < 29999 & zahlenF[k] > 10000) {
                    k++;
                    gewinn.feld[i][j] = "melone";
                    gp.add(new ImageView(melone), j, i);

                } else if (zahlenF[k] < 9999 & zahlenF[k] > 1) {
                    k++;
                    gewinn.feld[i][j] = "sieben";
                    gp.add(new ImageView(sieben), j, i);

                }

                //abstand
                gp.setVgap(10);
                gp.setHgap(10);

                //
            }
        }
        summe = gewinn.checkGewinn();
        return gp;
    }

}
