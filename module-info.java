module HalloApp {
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.swing;
    opens gui to javafx.fxml;
    exports gui;
}