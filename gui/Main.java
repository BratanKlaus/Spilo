package gui;

import backend.Gewinn;
import backend.KostenProSpin;
import backend.Spielfeld;
import gui.HauptFenster;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {


    @Override
    public void start(Stage stage) throws Exception {
        Gewinn gewinn = new Gewinn();
        Spielfeld spiel = new Spielfeld(gewinn);
        KostenProSpin ksp =new KostenProSpin(gewinn);
        HauptFenster hf = new HauptFenster(spiel,gewinn,ksp);
        hf.showView();
    }
}
