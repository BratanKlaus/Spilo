package gui;

import backend.Gewinn;
import backend.KostenProSpin;
import backend.Spielfeld;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

import javafx.stage.Stage;


public class HauptFenster extends Stage {


    KostenProSpin ksp;
    Spielfeld spiel;
    Gewinn gewinn;
    double summe;
    double guthaben;


    public HauptFenster(Spielfeld spiel, Gewinn gewinn, KostenProSpin ksp) {
        this.spiel = spiel;
        this.gewinn = gewinn;
        this.ksp = ksp;
    }


    public void showView() {
        setTitle("Bild wechsel dich ");

        Scene szene;
        BorderPane bp = new BorderPane();

        Button start = new Button("Start");
        TextField einsatz = new TextField("" + gewinn.getEinsatz());
        Button minBet = new Button("MinBet");
        Button maxBet = new Button("MaxBet");
        //anzeigen des guthabens
        TextField guthabenNow = new TextField(gewinn.getSumme() + "  Euro");
        einsatz.setPrefWidth(60);
        guthabenNow.setPrefWidth(80);

        einsatz.setEditable(false);
        guthabenNow.setEditable(false);
        //hbox
        HBox hb = new HBox();
        hb.setPadding(new Insets(10.0));
        hb.setSpacing(0.3);
        hb.setAlignment(Pos.CENTER);

        //Start Button
        start.setOnAction(e -> {
            if (ksp.checkGuthabenNotNull() == true) {
                ksp.proDrehKosten();
                bp.setCenter(spiel.tauschFeld());
                guthabenNow.setText(gewinn.getSumme() + " Euro");
            } else if (ksp.checkGuthabenNotNull() == false) {
                System.out.println("Guthaben Reicht nicht aus");
            }
        });
        //Maxbet button
        maxBet.setOnAction(e -> {
            gewinn.setMaxEinsatz();
            einsatz.setText("" + gewinn.getEinsatz());
        });
        //MinBet button
        minBet.setOnAction(e -> {
            gewinn.setEinsatz();
            einsatz.setText("" + gewinn.getEinsatz());
        });

        hb.getChildren().add(guthabenNow);
        hb.getChildren().add(minBet);
        hb.getChildren().add(maxBet);
        hb.getChildren().add(einsatz);
        hb.getChildren().add(start);

        hb.setPadding(new Insets(15));
        hb.setSpacing(25);

        bp.setBottom(hb);
        szene = new Scene(bp, 500, 500);

        setScene(szene);
        this.show();
    }
}
